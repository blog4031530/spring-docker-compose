package com.example.spring.dockercompose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDockerComposeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDockerComposeApplication.class, args);
	}

}
