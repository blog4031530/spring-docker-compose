package com.example.spring.dockercompose.dto;

import com.example.spring.dockercompose.entity.Member;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record MemberDto(@JsonProperty("id") Long id,
						@JsonProperty("userName") String userName,
						@JsonProperty("age") Integer age) {

	public Member entity() {
		return Member.builder()
			.userName(userName)
			.age(age)
			.build();
	}
}
