package com.example.spring.dockercompose.controller;

import com.example.spring.dockercompose.dto.MemberDto;
import com.example.spring.dockercompose.entity.Member;
import com.example.spring.dockercompose.repository.MemberRepository;
import com.example.spring.dockercompose.service.ProducerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class MemberController {
	private final MemberRepository memberRepository;
	private final ProducerService producerService;

	@GetMapping("/member/{id}")
	public ResponseEntity<Member> getMember(@PathVariable Long id) {
		Optional<Member> optionalMember = memberRepository.findById(id);
		Member member = optionalMember.orElseThrow(NoSuchElementException::new);
		return ResponseEntity.ok(member);
	}

	@PostMapping("/member")
	public void postMember(@RequestBody MemberDto memberDto) {
		producerService.produce(memberDto);
	}
}
