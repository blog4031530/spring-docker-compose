package com.example.spring.dockercompose.service;

import com.example.spring.dockercompose.config.TopicConfig;
import com.example.spring.dockercompose.dto.MemberDto;
import com.example.spring.dockercompose.entity.Member;
import com.example.spring.dockercompose.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.adapter.ConsumerRecordMetadata;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConsumerService {
	private final MemberRepository memberRepository;

	@KafkaListener(
		topics = TopicConfig.TOPIC_NAME,
		clientIdPrefix = "topic1-listener",
		groupId = "${spring.kafka.consumer.group-id}"
	)
	public void listen(MemberDto memberDto, ConsumerRecordMetadata metadata) {
		log.info("received message: {}", memberDto);
		log.info("received topic: {}", metadata.topic());
		log.info("received partition: {}", metadata.partition());
		log.info("received offset: {}", metadata.offset());
		Member saved = memberRepository.save(memberDto.entity());
		log.info("saved member {}", saved);
	}
}
