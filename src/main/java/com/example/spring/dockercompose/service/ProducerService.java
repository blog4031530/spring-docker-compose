package com.example.spring.dockercompose.service;

import com.example.spring.dockercompose.config.TopicConfig;
import com.example.spring.dockercompose.dto.MemberDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProducerService {
	private final KafkaTemplate<String, Object> kafkaTemplate;

	public void produce(final MemberDto memberDto) {
		String key = UUID.randomUUID().toString();
		kafkaTemplate.send(TopicConfig.TOPIC_NAME, key, memberDto)
			.whenComplete((result, throwable) -> {
				if (throwable != null) {
					log.error("fail to send message, {}", throwable.getMessage());
				} else {
					RecordMetadata metadata = result.getRecordMetadata();
					log.info("send message: {}", memberDto);
					log.info("send topic: {}", metadata.topic());
					log.info("send partition: {}", metadata.partition());
					log.info("send offset: {}", metadata.offset());
				}
			});
	}
}
